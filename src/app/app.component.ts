import { Component } from '@angular/core';
import {CommonModule, registerLocaleData} from '@angular/common';
import { RouterOutlet } from '@angular/router';
import {TaskItemService} from "./task-item.service";
import localeDe from "@angular/common/locales/de"
import {TaskItem} from "./task-item";
import {TaskItemComponent} from "./task-item/task-item.component";
import {TaskItemFormComponent} from "./task-item-form/task-item-form.component";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, TaskItemComponent, TaskItemFormComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'todo-list';

  constructor(private readonly taskItemService: TaskItemService) {
    registerLocaleData(localeDe);
  }

  getOpenTasks(): TaskItem[] {
    return this.taskItemService.getOpenTasks();
  }

  getClosedTasks(): TaskItem[] {
    return this.taskItemService.getClosedTasks();
  }
}
