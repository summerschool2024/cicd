import {Component, Input} from '@angular/core';
import {TaskItem} from "../task-item";
import {DatePipe, NgClass} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {TaskItemService} from "../task-item.service";
import {TaskItemFormComponent} from "../task-item-form/task-item-form.component";

@Component({
  selector: 'app-task-item',
  standalone: true,
  imports: [NgClass, DatePipe, FormsModule, TaskItemFormComponent],
  templateUrl: './task-item.component.html',
  styleUrl: './task-item.component.scss'
})
export class TaskItemComponent {

  @Input() taskItem: TaskItem | undefined;

  hovered= false;

  editing = false;

  constructor(private readonly taskItemService: TaskItemService) {}
  /**
   * Diese Methode prüft, ob das Datum des TaskItems (der Komponente) abgelaufen ist (= früher als der aktuelle
   * Zeitstempel)
   */
  isOverDue(): boolean {
    if (this.taskItem?.dueDate) {
      const now = new Date();
      return this.taskItem.dueDate.getTime() < now.getTime();
    }
    return false;
  }

  toggleDone() {
    if (this.taskItem) {
      this.taskItem.done = !this.taskItem.done;
      this.taskItemService.persistTaskItems();
    }
  }

  startEditing(event: Event) {
    event.stopPropagation();
    this.editing = true;
  }

  removeTaskItem(event: Event) {
    event.stopPropagation();
    if (this.taskItem) {
      this.taskItemService.removeTaskItem(this.taskItem);
    }
  }


}
